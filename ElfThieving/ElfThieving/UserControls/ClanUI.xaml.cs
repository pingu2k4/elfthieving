﻿using ElfThieving.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ElfThieving.UserControls
{
    /// <summary>
    /// Interaction logic for Clan.xaml
    /// </summary>
    public partial class ClanUI : UserControl
    {
        public ClanUI()
        {
            InitializeComponent();
        }

        public object Clan
        {
            get { return (object)GetValue(ClanProperty); }
            set { SetValue(ClanProperty, value); }
        }
        public static readonly DependencyProperty ClanProperty =
            DependencyProperty.Register("Clan", typeof(object),
            typeof(ClanUI), new PropertyMetadata(null));
    }
}
