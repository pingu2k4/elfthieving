﻿using ElfThieving.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Data;
using System.Windows.Input;

namespace ElfThieving.Models
{
    public class Clan : INotifyPropertyChanged
    {
        public Clan()
        {
            SetCallbacks();
        }

        #region HELPER FUNCTIONS
        private void SetCallbacks()
        {
            VoicedChanged += OnXPUpdateRequired;
            BaseXPChanged += OnXPUpdateRequired;
        }

        private void OnXPUpdateRequired()
        {
            if(Voiced)
            {
                XP = Convert.ToInt32(BaseXP * 1.2);
            }
            else
            {
                XP = BaseXP;
            }
        }

        public void StartTimer()
        {
            T = new Timer(1000);
            TS = new TimeSpan(0, 20, 0);

            T.Elapsed += T_Elapsed;
            UpdateTimerText();

            T.Start();

            TimerRunning = true;
            CurrentLocation = false;
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(TS.TotalSeconds > 0)
            {
                TS = TS.Subtract(TS1S);
            }
            else
            {
                StopTimer();
            }
            UpdateTimerText();
        }

        private void UpdateTimerText()
        {
            TimerText = "STOP: " + TS.ToString(@"mm\:ss");
        }

        public void StopTimer()
        {
            T.Stop();
            TimerRunning = false;
        }
        #endregion

        #region PROPERTIES
        public delegate void MultiDelegate();

        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _Image;
        public string Image
        {
            get
            {
                return _Image;
            }
            set
            {
                _Image = value;
                OnPropertyChanged("Image");
            }
        }

        private int _ThievingLevel;
        public int ThievingLevel
        {
            get
            {
                return _ThievingLevel;
            }
            set
            {
                _ThievingLevel = value;
                OnPropertyChanged("ThievingLevel");
            }
        }

        private bool _Active = false;
        public bool Active
        {
            get
            {
                return _Active;
            }
            set
            {
                _Active = value;
                OnPropertyChanged("Active");
            }
        }

        private bool _Hilighted = false;
        public bool Hilighted
        {
            get
            {
                return _Hilighted;
            }
            set
            {
                _Hilighted = value;
                OnPropertyChanged("Hilighted");
            }
        }

        public MultiDelegate VoicedChanged;
        private bool _Voiced = false;
        public bool Voiced
        {
            get
            {
                return _Voiced;
            }
            set
            {
                if (_Voiced != value)
                {
                    _Voiced = value;
                    OnPropertyChanged("Voiced");
                    if (VoicedChanged != null)
                    {
                        VoicedChanged();
                    }
                }
            }
        }

        public MultiDelegate BaseXPChanged;
        private int _BaseXP;
        public int BaseXP
        {
            get
            {
                return _BaseXP;
            }
            set
            {
                if (_BaseXP != value)
                {
                    _BaseXP = value;
                    OnPropertyChanged("BaseXP");
                    if (BaseXPChanged != null)
                    {
                        BaseXPChanged();
                    }
                }
            }
        }

        private int _XP;
        public int XP
        {
            get
            {
                return _XP;
            }
            set
            {
                _XP = value;
                OnPropertyChanged("XP");
            }
        }

        public MultiDelegate TimerRunningChanged;
        private bool _TimerRunning;
        public bool TimerRunning
        {
            get
            {
                return _TimerRunning;
            }
            set
            {
                if (_TimerRunning != value)
                {
                    _TimerRunning = value;
                    OnPropertyChanged("TimerRunning");
                    if (TimerRunningChanged != null)
                    {
                        TimerRunningChanged();
                    }
                }
            }
        }

        private string _TimerText;
        public string TimerText
        {
            get
            {
                return _TimerText;
            }
            set
            {
                _TimerText = value;
                OnPropertyChanged("TimerText");
            }
        }

        Timer T;
        TimeSpan TS;
        private TimeSpan TS1S = new TimeSpan(0, 0, 1);
        
        private bool _CurrentLocation = false;
        public bool CurrentLocation
        {
            get
            {
                return _CurrentLocation;
            }
            set
            {
                _CurrentLocation = value;
                OnPropertyChanged("CurrentLocation");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
