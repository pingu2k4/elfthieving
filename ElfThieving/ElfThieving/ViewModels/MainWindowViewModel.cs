﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ElfThieving.Commands;
using ElfThieving.Models;
using ElfThieving.Views;
using ElfThieving.UserControls;

namespace ElfThieving.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();
            CheckParameters();
            SetPanels();
            SetCallbacks();
            LoadSettings();
            SetupClans();
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }

        private void CheckParameters()
        {
            if (Application.Current.Properties["FileName"] != null)
            {
                //We opened up with parameters - Handle this here.
            }
        }

        private void SetPanels()
        {
            MainWindowContentControl = MainPanelUserControl;
        }

        private void SetCallbacks()
        {
            ThievingLevelChanged += OnThievingLEvelChanged; 
        }

        private void LoadSettings()
        {
            ThievingLevel = Properties.Settings.Default.ThievingLevel;
        }

        private void SetupClans()
        {
            Meilyr.Name = "Meilyr";
            Meilyr.Image = "../Media/Clans/Meilyr.png";
            Meilyr.ThievingLevel = 98;
            Meilyr.BaseXP = 170;
            Meilyr.TimerRunningChanged += SetClanHilighted;

            Crwys.Name = "Crwys";
            Crwys.Image = "../Media/Clans/Crwys.png";
            Crwys.ThievingLevel = 97;
            Crwys.BaseXP = 155;
            Crwys.TimerRunningChanged += SetClanHilighted;

            Cadarn.Name = "Cadarn";
            Cadarn.Image = "../Media/Clans/Cadarn.png";
            Cadarn.ThievingLevel = 93;
            Cadarn.BaseXP = 135;
            Cadarn.TimerRunningChanged += SetClanHilighted;

            Trahaearn.Name = "Trahaearn";
            Trahaearn.Image = "../Media/Clans/Trahaearn.png";
            Trahaearn.ThievingLevel = 95;
            Trahaearn.BaseXP = 145;
            Trahaearn.TimerRunningChanged += SetClanHilighted;

            Iorwerth.Name = "Iorwerth";
            Iorwerth.Image = "../Media/Clans/Iorwerth.png";
            Iorwerth.ThievingLevel = 91;
            Iorwerth.BaseXP = 125;
            Iorwerth.TimerRunningChanged += SetClanHilighted;

            Ithell.Name = "Ithell";
            Ithell.Image = "../Media/Clans/Ithell.png";
            Ithell.ThievingLevel = 92;
            Ithell.BaseXP = 130;
            Ithell.TimerRunningChanged += SetClanHilighted;

            Amlodd.Name = "Amlodd";
            Amlodd.Image = "../Media/Clans/Amlodd.png";
            Amlodd.ThievingLevel = 94;
            Amlodd.BaseXP = 140;
            Amlodd.TimerRunningChanged += SetClanHilighted;

            Hefin.Name = "Hefin";
            Hefin.Image = "../Media/Clans/Hefin.png";
            Hefin.ThievingLevel = 96;
            Hefin.BaseXP = 150;
            Hefin.TimerRunningChanged += SetClanHilighted;

            Clans.Add(Meilyr);
            Clans.Add(Crwys);
            Clans.Add(Cadarn);
            Clans.Add(Trahaearn);
            Clans.Add(Iorwerth);
            Clans.Add(Ithell);
            Clans.Add(Amlodd);
            Clans.Add(Hefin);

            SetClanActiveStates();
            SetClanHilighted();
        }
        #endregion

        #region HELPER FUNCTIONS
        #region CALLBACKS
        private void OnThievingLEvelChanged()
        {
            Properties.Settings.Default.ThievingLevel = ThievingLevel;
            Properties.Settings.Default.Save();
            SetClanActiveStates();
            SetClanHilighted();
        }
        #endregion
        #region HELPERS
        private void SetClanActiveStates()
        {
            foreach (Clan C in Clans)
            {
                if(C.ThievingLevel > ThievingLevel)
                {
                    C.Active = false;
                }
                else
                {
                    C.Active = true;
                }
            }
        }

        private void SetClanHilighted()
        {
            foreach (Clan C in Clans)
            {
                C.Hilighted = false;
            }

            Clan CH = null;
            Clan CH2 = null;
            int Max = 0;
            foreach (Clan C in Clans.FindAll(C => C.Active && !C.TimerRunning))
            {
                if(C.XP > Max)
                {
                    CH = C;
                    Max = C.XP;
                    CH2 = null;
                }
                else if(C.XP == Max)
                {
                    CH2 = C;
                }
            }

            if(CH != null)
            {
                CH.Hilighted = true;
            }

            if (CH2 != null)
            {
                CH2.Hilighted = true;
            }
        }
        #endregion
        #region MISC
        private void Save()
        {
            //if (FileName == "")
            //{
            //    Status = "No file open! Open or create a file first.";
            //    return;
            //}
            //if (!File.Exists(Path\FileName))
            //{
            //    Status = "File does not exist!";
            //    return;
            //}

            SaveFile SF = GenerateSaveFile();

            JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            string SFString = JsonConvert.SerializeObject(SF, Formatting.Indented, Settings);

            //using (StreamWriter SW = new StreamWriter(Path\FileName))
            //{
            //    SW.Write(SFString);
            //}
            //using (StreamWriter SW = new StreamWriter(Path\FileName))
            //{
            //    SW.Write(SFString);
            //}

            //DirtyFile = false;
        }

        private void Load()
        {
            //try
            //{
            //    string SFString = "";
            //    bool SFRecovered = false;
            //    using (StreamReader SR = new StreamReader(GetQuestFileName(FileName)))
            //    {
            //        SFString = SR.ReadToEnd();
            //    }

            //    SaveFile SF = new SaveFile();

            //    JsonSerializerSettings Settings = new JsonSerializerSettings
            //    {
            //        TypeNameHandling = TypeNameHandling.All
            //    };

            //    SF = JsonConvert.DeserializeObject<SaveFile>(SFString, Settings);

            //    if (File.Exists(GetQuestBackupFileName(FileName)))
            //    {
            //        if (!FilesAreEqual(new FileInfo(GetQuestFileName(FileName)), new FileInfo(GetQuestBackupFileName(FileName))))
            //        {
            //            MessageBoxResult DR = MessageBox.Show("There is a backup to this quest with more recent, unsaved changes. Would you like to recover this information? (Warning: This will overwrite your current save for the quest).", "Autorecovery", MessageBoxButton.YesNo);

            //            if (DR == MessageBoxResult.Yes)
            //            {
            //                string SFBackupString = "";
            //                SaveFile SFBackup = null;
            //                using (StreamReader SR = new StreamReader(GetQuestBackupFileName(FileName)))
            //                {
            //                    SFBackupString = SR.ReadToEnd();
            //                }

            //                if (SFBackupString != "")
            //                {
            //                    SFBackup = JsonConvert.DeserializeObject<SaveFile>(SFBackupString);
            //                }

            //                SF = SFBackup;
            //                SFRecovered = true;
            //            }
            //        }
            //    }

            //    CurrentQuestFileName = FileName;

            //    ExtractSaveFile(SF);

            //    if (SFRecovered)
            //        SaveOpenQuest();

            //    QuestFileOpen = true;
            //    CurrentQuestDirty = false;
            //}
            //catch (Exception Ex)
            //{
            //    Status = Ex.Message;
            //}
        }

        private SaveFile GenerateSaveFile()
        {
            SaveFile SF = new SaveFile();

            return SF;
        }

        private void ExtractSaveFile()
        {

        }
        #endregion
        #endregion

        #region PROPERTIES
        public delegate void MultiDelegate();
        #region PANELS
        private UserControl _MainWindowContentControl;
        public UserControl MainWindowContentControl
        {
            get
            {
                return _MainWindowContentControl;
            }
            set
            {
                if (_MainWindowContentControl != value)
                {
                    _MainWindowContentControl = value;
                    OnPropertyChanged("MainWindowContentControl");
                }
            }
        }

        private MainPanel MainPanelUserControl = new MainPanel();

        #endregion
        #region PROPERTIES.SETTINGS
        public MultiDelegate ThievingLevelChanged;
        private int _ThievingLevel;
        public int ThievingLevel
        {
            get
            {
                return _ThievingLevel;
            }
            set
            {
                if (_ThievingLevel != value)
                {
                    _ThievingLevel = value;
                    OnPropertyChanged("ThievingLevel");
                    if (ThievingLevelChanged != null)
                    {
                        ThievingLevelChanged();
                    }
                }
            }
        }
        #endregion
        #region CLANS
        private Clan _Meilyr = new Clan();
        public Clan Meilyr
        {
            get
            {
                return _Meilyr;
            }
            set
            {
                _Meilyr = value;
                OnPropertyChanged("Meilyr");
            }
        }

        private Clan _Crwys = new Clan();
        public Clan Crwys
        {
            get
            {
                return _Crwys;
            }
            set
            {
                _Crwys = value;
                OnPropertyChanged("Crwys");
            }
        }

        private Clan _Cadarn = new Clan();
        public Clan Cadarn
        {
            get
            {
                return _Cadarn;
            }
            set
            {
                _Cadarn = value;
                OnPropertyChanged("Cadarn");
            }
        }

        private Clan _Trahaearn = new Clan();
        public Clan Trahaearn
        {
            get
            {
                return _Trahaearn;
            }
            set
            {
                _Trahaearn = value;
                OnPropertyChanged("Trahaearn");
            }
        }

        private Clan _Iorwerth = new Clan();
        public Clan Iorwerth
        {
            get
            {
                return _Iorwerth;
            }
            set
            {
                _Iorwerth = value;
                OnPropertyChanged("Iorwerth");
            }
        }

        private Clan _Ithell = new Clan();
        public Clan Ithell
        {
            get
            {
                return _Ithell;
            }
            set
            {
                _Ithell = value;
                OnPropertyChanged("Ithell");
            }
        }

        private Clan _Amlodd = new Clan();
        public Clan Amlodd
        {
            get
            {
                return _Amlodd;
            }
            set
            {
                _Amlodd = value;
                OnPropertyChanged("Amlodd");
            }
        }

        private Clan _Hefin = new Clan();
        public Clan Hefin
        {
            get
            {
                return _Hefin;
            }
            set
            {
                _Hefin = value;
                OnPropertyChanged("Hefin");
            }
        }

        private List<Clan> _Clans = new List<Clan>();
        public List<Clan> Clans
        {
            get
            {
                return _Clans;
            }
            set
            {
                _Clans = value;
                OnPropertyChanged("Clans");
            }
        }

        #endregion
        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion _ProgramVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return _ProgramVersion.Version;
            }
        }

        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    OnPropertyChanged("Status");
                }
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region CLAN
        private ICommand _ClanVoiced;
        public ICommand ClanVoiced
        {
            get
            {
                if (_ClanVoiced == null)
                {
                    _ClanVoiced = new RelayCommand(ClanVoicedEx, null);
                }
                return _ClanVoiced;
            }
        }
        private void ClanVoicedEx(object p)
        {
            Clans.First(C => C.Name == p.ToString()).Voiced = !Clans.First(C => C.Name == p.ToString()).Voiced;

            SetClanHilighted();
        }
        
        private ICommand _ClanTimer;
        public ICommand ClanTimer
        {
            get
            {
                if (_ClanTimer == null)
                {
                    _ClanTimer = new RelayCommand(ClanTimerEx, null);
                }
                return _ClanTimer;
            }
        }
        private void ClanTimerEx(object p)
        {
            Clan C = Clans.First(P => P.Name == p.ToString());

            if(C == null)
            {
                return;
            }

            if(C.TimerRunning)
            {
                C.StopTimer();
            }
            else
            {
                C.StartTimer();
            }

            SetClanHilighted();
        }


        private ICommand _ClanCurrentLocationSet;
        public ICommand ClanCurrentLocationSet
        {
            get
            {
                if (_ClanCurrentLocationSet == null)
                {
                    _ClanCurrentLocationSet = new RelayCommand(ClanCurrentLocationSetEx, null);
                }
                return _ClanCurrentLocationSet;
            }
        }
        private void ClanCurrentLocationSetEx(object p)
        {
            foreach (Clan C in Clans.FindAll(P => P.CurrentLocation))
            {
                C.CurrentLocation = false;
            }

            Clans.First(C => C.Name == p.ToString()).CurrentLocation = true;
        }


        private ICommand _ClanNext;
        public ICommand ClanNext
        {
            get
            {
                if (_ClanNext == null)
                {
                    _ClanNext = new RelayCommand(ClanNextEx, null);
                }
                return _ClanNext;
            }
        }
        private void ClanNextEx(object p)
        {
            Clan CN = Clans.Find(C => C.CurrentLocation);
            if (CN != null)
            {
                ClanTimerEx(CN.Name);
            }

            Clan CH = Clans.Find(C => C.Hilighted);
            if(CH != null)
            {
                ClanCurrentLocationSetEx(CH.Name);
            }
        }
        #endregion
        #region MISC
        private ICommand _OpenPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (_OpenPreferences == null)
                {
                    _OpenPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return _OpenPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }

        public void WindowClosing(CancelEventArgs e)
        {
            //Mark this as true if you wish to potentiolly cancel the window closing.
            if (false)
            {
                MessageBoxResult DR = MessageBox.Show("BODY", "TITLE", MessageBoxButton.YesNoCancel);

                switch (DR)
                {
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.Yes:
                        Save();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }
            }
        }

        private ICommand _ThemedDialog;
        public ICommand ThemedDialog
        {
            get
            {
                if (_ThemedDialog == null)
                {
                    _ThemedDialog = new RelayCommand(ThemedDialogEx, null);
                }
                return _ThemedDialog;
            }
        }
        private void ThemedDialogEx(object p)
        {
            DialogWindow DialogWindow = new DialogWindow("Title", "Message", "PRESS ME", null);
            DialogWindow.Owner = p as MainWindow;
            DialogWindow.ShowDialog();
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
